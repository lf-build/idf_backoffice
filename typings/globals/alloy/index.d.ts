import IBehaviors = alloy.IBehaviors;
declare namespace alloy {
    import PolymerStatic = polymer.PolymerStatic;
    interface IConfig {
        base: string;
        appToken: string;
        "loan-fees": Object;
        filters: Object;
        "log-activity": Object;
        login: string;
        onboarding: Object;
        services: IServices;
    }

    interface IServices {
        security: string;
        queue: string;
        loan: string;
        loanActivityLog: string;
        eventHub: string;
        amortization: string;
        search: string;
        configuration: string;
        inbox: string;
        calendar: string;
        loanFilters: string;
        payment: string;
        loanStatusManagement: string;
        appRegistry: string;
        decisionEngine: string;
        appFlow: string;
        actionCenter: string;
        merchant: string;
        emailVerification: string;
        businessReport: string;
        merchantFilter: string;
        applicationFilters: string;
        consentTracker: string;
        offerEngine: string;
    }

    interface I$httpRequestService extends polymer.Base {
        get(): Promise<any>;
        post(): Promise<any>;
        put(): Promise<any>;
    }

    interface IBehaviors {
        userManagementActivity: IUserManagementActivity;
    }

    interface IUserManagementActivity {
        BForm: Object;
        BActivity: Object;
        BFormElement: Object;
        BFormInput: Object;
        BFormErrors: Object;
    }
}

interface Window {
    config?: alloy.IConfig;
    behaviors: IBehaviors;
}

declare var config: alloy.IConfig;
declare var window: Window;