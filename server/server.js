var express = require('express');
var app = express();
var jwt = require('express-jwt');
var bodyParser = require('body-parser');
var path = require('path');

var routes = require(__dirname + '/routes.js');
var s3Routes = require(__dirname + '/s3.js');

// CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

// parse application/json
app.use(bodyParser.json())

// parse an HTML body into a string
app.use(bodyParser.text({ type: 'text/html' }))

// node server.js local
var local = process.argv.indexOf('local') !== -1;
var environment = process.env.environment;

if (environment === 'local') {
    var pathToServe = path.join(__dirname, '..','app', 'components');
    console.log('Serving : ' + pathToServe);
    app.use('/app', express.static(pathToServe));

    var pathToServe = path.join(__dirname, '..','app', 'components', 'lendfoundry');
    console.log('Serving : ' + pathToServe);
    app.use('/app/my-tenant', express.static(pathToServe));

    // Below written module does poor file serving
    // it servers files but always sends mime type as HTML.
    // app.use('/app', routes);
} else if (environment === 's3') {
    app.use('/app', s3Routes);
} else {
    console.warn('Valid environment not set!');
}

app.listen(process.env.port || 5000, function() {
    console.log('listening on port ' + (process.env.port || 5000));
});