export const SET_AMOUNT = "set-amount";
export const SET_REASON = "set-reason";
export const SET_APPLICANT_BASIC = "set-applicant-basic";
export const SET_APPLICANT_PERSONAL = "set-applicant-personal";
export const SET_EMPLOYMENT = "set-employment";
export const GENERATE_MOBILE_OTP = "generate-mobile-otp";
export const VERIFY_MOBILE_OTP = "verify-mobile-otp";
export const GENERATE_EMAIL_OTP = "generate-email-otp";
export const VERIFY_EMAIL_OTP = "verify-email-otp";


export function setAmount(amount){
    if(!amount || typeof amount !== 'number' || amount <= 0){
        throw 'amount should be a positive number';
    }
    return {
        type: SET_AMOUNT,
        amount
    };
}

export function setReason(reason){
    if(!reason || typeof reason !== 'string'){
        throw '';
    }
}

export function setApplicantBasic(applicantBasic){
    if(!applicantBasic || typeof applicantBasic !== 'object'){
        throw '';
    }
}

export function setApplicantPersonal(applicantPersonal){
    if(!applicantPersonal || typeof applicantPersonal !== 'object'){
        throw '';
    }
}

export function setEmployment(employment){
    if(!employment || typeof employment !== 'object'){
        throw '';
    }
}

export function generateMobileOTP(mobile){
    if(!mobile || typeof mobile !== 'string'){
        throw '';
    }
}

export function verifyMobileOTP(mobile, otp){
    if(!mobile || !otp || typeof mobile !== 'string' || typeof otp !== 'string'){
        throw '';
    }
}

export function generateEmailOTP(email){
    if(!email || typeof email !== 'string'){
        throw '';
    }
}

export function verifyEmailOTP(email, otp){
    if(!email || !otp || typeof email !== 'string' || typeof otp !== 'string'){
        throw '';
    }
}