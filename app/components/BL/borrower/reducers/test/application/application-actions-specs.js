import * as types from '../../application/application-actions';

var expect = require('chai').expect;
 

describe ('The Application has ', () => {
    
    describe ('SET_AMOUNT action', () => {
        it ('with defined constant SET_AMOUNT', () => {
            expect(types.SET_AMOUNT).to.equal('set-amount');
        });
        
        describe ('with action builder which', () => {
            it ('accepts positive numbers', () => {
                expect(types.setAmount.bind(null, 1)).to.not.throw();                                
            });
            it ('reject amounts less then or equal to zero', () => {
                //Throw when number out of range or non numberic
                expect(types.setAmount.bind(null, 0)).to.throw('amount should be a positive number');                
                expect(types.setAmount.bind(null, -1)).to.throw('amount should be a positive number');                
            });
            it ('reject empty inputs', () => {
                expect(types.setAmount.bind(null)).to.throw('amount should be a positive number');                
                expect(types.setAmount.bind(null, null)).to.throw('amount should be a positive number');
            });
            it ('reject non numeric inputs', () => {
                expect(types.setAmount.bind(null, "123")).to.throw('amount should be a positive number');
                expect(types.setAmount.bind(null, { amount: 123 })).to.throw('amount should be a positive number');                
            });
        });
    });

    describe ('SET_REASON action', () => {
        it ('with defined constant SET_REASON', () => {
            expect(types.SET_REASON).to.equal('set-reason');
        });
        
        describe ('with action builder which', () => {
            it ('reject empty inputs', () => {
                expect(types.setReason.bind(null)).to.throw();                
                expect(types.setReason.bind(null, null)).to.throw();
            });
        });
    });

    describe ('SET_APPLICANT_BASIC action', () => {
        it ('with defined constant SET_APPLICANT_BASIC', () => {
            expect(types.SET_APPLICANT_BASIC).to.equal('set-applicant-basic');
        });
        
        describe ('with action builder which', () => {
            it ('reject empty inputs', () => {
                expect(types.setApplicantBasic.bind(null)).to.throw();                
                expect(types.setApplicantBasic.bind(null, null)).to.throw();
            });
        });
    });

    describe ('SET_APPLICANT_PERSONAL action', () => {
        it ('with defined constant SET_APPLICANT_PERSONAL', () => {
            expect(types.SET_APPLICANT_PERSONAL).to.equal('set-applicant-personal');
        });
        
        describe ('with action builder which', () => {
            it ('reject empty inputs', () => {
                expect(types.setApplicantPersonal.bind(null)).to.throw();                
                expect(types.setApplicantPersonal.bind(null, null)).to.throw();
            });
        });
    });

    describe ('SET_EMPLOYMENT action', () => {
        it ('with defined constant SET_EMPLOYMENT', () => {
            expect(types.SET_EMPLOYMENT).to.equal('set-employment');
        });
        
        describe ('with action builder which', () => {
            it ('reject empty inputs', () => {
                expect(types.setEmployment.bind(null)).to.throw();                
                expect(types.setEmployment.bind(null, null)).to.throw();
            });
        });
    });

    describe ('GENERATE_MOBILE_OTP action', () => {
        it ('with defined constant GENERATE_MOBILE_OTP', () => {
            expect(types.GENERATE_MOBILE_OTP).to.equal('generate-mobile-otp');
        });
        
        describe ('with action builder which', () => {
            it ('reject empty inputs', () => {
                expect(types.generateMobileOTP.bind(null)).to.throw();                
                expect(types.generateMobileOTP.bind(null, null)).to.throw();
            });
        });
    }); 

    describe ('VERIFY_MOBILE_OTP action', () => {
        it ('with defined constant VERIFY_MOBILE_OTP', () => {
            expect(types.VERIFY_MOBILE_OTP).to.equal('verify-mobile-otp');
        });
        
        describe ('with action builder which', () => {
            it ('reject empty inputs', () => {
                expect(types.verifyMobileOTP.bind(null)).to.throw();                
                expect(types.verifyMobileOTP.bind(null, null)).to.throw();
            });
        });
    });

    describe ('GENERATE_EMAIL_OTP action', () => {
        it ('with defined constant GENERATE_EMAIL_OTP', () => {
            expect(types.GENERATE_EMAIL_OTP).to.equal('generate-email-otp');
        });
        
        describe ('with action builder which', () => {
            it ('reject empty inputs', () => {
                expect(types.generateEmailOTP.bind(null)).to.throw();                
                expect(types.generateEmailOTP.bind(null, null)).to.throw();
            });
        });
    });

    describe ('VERIFY_EMAIL_OTP action', () => {
        it ('with defined constant VERIFY_EMAIL_OTP', () => {
            expect(types.VERIFY_EMAIL_OTP).to.equal('verify-email-otp');
        });
        
        describe ('with action builder which', () => {
            it ('reject empty inputs', () => {
                expect(types.verifyEmailOTP.bind(null)).to.throw();                
                expect(types.verifyEmailOTP.bind(null, null)).to.throw();
            });
        });
    });
   
});