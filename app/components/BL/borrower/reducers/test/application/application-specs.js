import * as types from '../../application/application-actions';

var expect = require('chai').expect;
var app = require('../../application/application');

describe ('The Application reducer', () => {
    
    it('returns default state when empty state is passed',() =>{
        expect(app(null, null)).not.to.equal(null);
        expect(app(null, null)).not.to.equal(undefined);
        expect(app(undefined, null)).not.to.equal(null);
        expect(app(undefined, null)).not.to.equal(undefined);
    });

   it('returns current state when empty action is passed',() =>{
       var initialState = { };
        expect(app(initialState, null)).to.equal(initialState);
        expect(app(initialState, null)).to.equal(initialState);
    });

    describe('handles SET_AMOUNT action and', () => {
        // beforeEach(() => {
        //     console.log('before');
        // });
        describe('rejects when amount is',() => {
            it('empty', () => {
                var initialState = { };
                expect(app(initialState,))
            });
            it('non numric');
            it('negetive number');
        });
        describe('accepts when amount is ',() => {
            it('positive number');
        });
        
    });

    describe('handles SET_REASON action',()=>{});
    describe('handles SET_APPLICANT_BASIC action',()=>{});
    describe('handles SET_APPLICANT_PERSONAL action',()=>{});
    describe('handles SET_EMPLOYMENT action',()=>{});
    describe('handles GENERATE_MOBILE_OTP action',()=>{});
    describe('handles VERIFY_MOBILE_OTP action',()=>{});
    describe('handles GENERATE_EMAIL_OTP action',()=>{});
    describe('handles VERIFY_EMAIL_OTP action',()=>{});
});