(function () {

  let $httpRequest: alloy.I$httpRequestService = {
    is: "http-request-promisified",

    get() {
      return Promise.resolve(this.$.http.get.apply(this.$.http, arguments));
    },

    post() {
      return Promise.resolve(this.$.http.post.apply(this.$.http, arguments));
    },

    put() {
      return Promise.resolve(this.$.http.put.apply(this.$.http, arguments));
    },

    delete() {
      return Promise.resolve(this.$.http.delete.apply(this.$.http, arguments));
    }
  };

  Polymer($httpRequest);
})();